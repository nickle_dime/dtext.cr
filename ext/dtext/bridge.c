#include "dtext.h"
#include <stdlib.h>
#include <string.h>

// could've linked directly to dtext.c and skipped this, but this way we don't have to interface with GLib in Crystal

int render_dtext(char* input, char** output, char** error,
                 bool strip, bool f_inline, bool f_mentions, bool f_color, long f_max_thumbs) {
    StateMachine* sm = init_machine(input, strlen(input), strip, f_inline, f_mentions, f_color, f_max_thumbs);

    int ret = 0;

    if (!parse_helper(sm)) {
        *error = g_strdup(sm->error->message);
        ret = -1;
    } else {
        *output = g_strdup(sm->output->str);
    }
    
    free_machine(sm);
    return ret;
}