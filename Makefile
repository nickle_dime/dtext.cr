CFLAGS ?= $(shell pkg-config --cflags glib-2.0)
LIBS ?= $(shell pkg-config --libs glib-2.0)

EXT := ext/dtext
DTEXT := $(EXT)/libdtext.so

$(DTEXT): $(EXT)/dtext.c $(EXT)/bridge.c
	$(CC) $(CFLAGS) -fPIC -c $(EXT)/dtext.c -o $(EXT)/dtext.o
	$(CC) $(CFLAGS) -fPIC -c $(EXT)/bridge.c -o $(EXT)/bridge.o
	$(CC) $(LIBS) -shared $(EXT)/dtext.o $(EXT)/bridge.o -o $(DTEXT)

$(EXT)/dtext.c: $(EXT)/dtext.rl
	ragel $(EXT)/dtext.rl -o $(EXT)/dtext.c

clean:
	rm $(EXT)/dtext.o $(EXT)/bridge.o $(DTEXT)

.PHONY: clean