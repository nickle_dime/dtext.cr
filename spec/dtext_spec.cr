require "./spec_helper"

describe DText do
  # TODO: Write tests

  it "works" do
    DText.render("[b]hello, I am [i]italicized![/i][/b]").should eq "<p><strong>hello, I am <em>italicized!</em></strong></p>"
  end

  it "has the correct link base" do
    DText.render("blip #12345").should contain %(href="https://e621.net)
  end

  it "errors on invalid UTF-8" do
    begin
      DText.render("[i]bad time\xc3\x28")
    rescue err : DText::ParseError
      err.message.should eq "invalid utf8 starting at byte 12"
    end
  end
end
