require "./libdtext"

module DText
  class ParseError < Exception
  end

  def self.render(input : String, strip = false, inline = false, mentions = false, color = false, max_thumbs = 25)
    status = LibDText.render_dtext(input, out output, out error, strip, inline, mentions, color, max_thumbs)
    # the Crystal GC takes over once the pointers are converted to Strings
    # we don't have to worry about free()ing them ourselves
    raise ParseError.new(String.new(error)) if status < 0
    String.new(output)
  end
end
