@[Link(ldflags: "#{__DIR__}/../ext/dtext/libdtext.so")]
lib LibDText
  fun render_dtext(input : LibC::Char*, output : LibC::Char**, error : LibC::Char**,
                   strip : Bool, inline : Bool, mentions : Bool, color : Bool, max_thumbs : LibC::Long) : LibC::Int
end
