# dtext.cr

A Crystal interface to the owobrowser fork of [e621's DText parser](https://github.com/zwagoth/dtext_rb).

## Dependencies

Requires [GLib 2](https://developer.gnome.org/glib/), a C compiler, and a `make` implementation.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     dtext:
       gitlab: nickle_dime/dtext.cr
   ```

2. Run `shards install`

## Usage

```crystal
require "dtext"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new merge request

## Contributors

- [Nick](https://gitlab.com/nickle_dime) - creator and maintainer
